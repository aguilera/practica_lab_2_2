from dos_menores import dos_menores

"sino hay nada no devuelve nada"
def test_A1():
    assert dos_menores() == None

"sino hay una lista vacia no devuelve nada"
def test_A2():
    assert dos_menores([]) == None

"si solo hay un numero devuelve ese numero y el 0"
def test_B():
    assert dos_menores([1]) == (1,0)

"si la entrada son dos numeros el primero mas pequeño que el segundo la salida sera igual"
def test_C12():
    assert dos_menores([1,2]) == (1,2)

"si el segundo numero es mas pequeño este sera el min 1 y el primero sera el min 2"
def test_21():
    assert dos_menores([2,1]) == (1,2)

"si hay tres numeros ordenados de menor a mayor devuelve los dos primeros"
def test_D():
   assert dos_menores([1,2,3]) == (1,2)

"si hay cuatro numeros ordenados de menor a mayor devuelve los dos primeros"
def test_E():
   assert dos_menores([1,2,3,4]) == (1,2)

"si hay tres numeros y el tercero es el mas pequeño y luego el primero devuelve el tercero y el primero"
def test_F():
   assert dos_menores([2,3,1]) == (1,2)

"si hay tres numeros y el primero es el mas pequeño y luego el tercero devuelve el primero y el tercero"
def test_G():
   assert dos_menores([1,3,2]) == (1,2)




