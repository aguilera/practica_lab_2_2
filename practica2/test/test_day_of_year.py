from day_of_year import day_of_year, InvalidArgument
import pytest


"sino hay nada devuelve una excepcion"
def test_A1():
    with pytest.raises(InvalidArgument):
        assert day_of_year(), "Deberıa haberse elevado la excepcion InvalidArgument"

"si hay una lista vacia devuelve una excepcion"
def test_A2():
    with pytest.raises(InvalidArgument):
        assert day_of_year([]), "Deberıa haberse elevado la excepcion InvalidArgument"

"si la entrada no es una fecha con el formato correcto"
def test_B():
    with pytest.raises(InvalidArgument):
        assert day_of_year(1), "Deberıa haberse elevado la excepcion InvalidArgument"

"si la entrada  es una fecha inexistente devuelve una excepcion"
def test_C():
    with pytest.raises(InvalidArgument):
        assert day_of_year(41,7,2001), "Deberıa haberse elevado la excepcion InvalidArgument"

"si funciona correctamente"
def test_D():
    assert day_of_year(1,1,2001) == (1)

"si es un año bisiesto el 29 de febrero"
def test_E():
    assert day_of_year(29,2,1904) == (60)

"si no es un año bisiesto el 29 de febrero"
def test_F():
    with pytest.raises(InvalidArgument):
        assert day_of_year(29,2,1905), "Deberıa haberse elevado la excepcion InvalidArgument"