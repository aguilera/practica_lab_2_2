from max_dist import max_dist, InvalidArgument
import pytest


"sino hay nada devuelve una excepcion"
def test_A1():
    with pytest.raises(InvalidArgument):
        assert max_dist(), "Deberıa haberse elevado la excepcion InvalidArgument"

"sino hay una lista vacia devuelve una excepcion"
def test_A2():
    with pytest.raises(InvalidArgument):
        assert max_dist([]), "Deberıa haberse elevado la excepcion InvalidArgument"

"si solo hay un numero devuelve una excepcion"
def test_B():
    with pytest.raises(InvalidArgument):
        assert max_dist([1]), "Deberıa haberse elevado la excepcion InvalidArgument"

"si solo hay dos numeros devuelve la distancia entre ellos"
def test_C():
    assert max_dist([1,3]) == (2)

"si la entrada no es un entero devuelve una excepcion"
def test_D():
    with pytest.raises(TypeError):
        assert max_dist([3.2, 2.5]), "Deberıa haberse elevado la excepcion TypeError"

"si  hay tres numeros devuelve la distancia mas grande entre los que esten contiguos"
def test_E():
    assert max_dist([1,3,4]) == (2)

"si  hay tres numeros devuelve la distancia mas grande entre los que esten contiguos"
def test_F():
    assert max_dist([1,3,9]) == (6)

"si  hay tres numeros devuelve la distancia mas grande en valor absoluto entre los que esten contiguos"
def test_G():
    assert max_dist([10,1,2]) == (9)

"si  hay tres numeros con la misma distancia devuelve esa distancia"
def test_H():
    assert max_dist([1, 1, 1]) == (0)
