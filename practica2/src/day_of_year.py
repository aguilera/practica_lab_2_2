class InvalidArgument(Exception):
    "Function called with invalid arguments"
    pass


def day_of_year(day=None, month=None, year=None):
    if not all(isinstance(arg, int) for arg in (day, month, year)):
        raise InvalidArgument

    days_per_month = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    if is_leap_year(year):
        days_per_month = [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    if month < 1 or month > 12 or day < 1 or day > days_per_month[month]:
        raise InvalidArgument

    days = day
    for i in range(1, month):
        days += days_per_month[i]

    return days

"funcion para tener en cuenta los años bisiestros"
def is_leap_year(year):
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)