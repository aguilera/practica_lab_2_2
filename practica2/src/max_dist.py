class InvalidArgument(Exception):
    "Function called with invalid arguments"
    pass


def max_dist(lst=None):
    if type(lst) != list or len(lst) < 2:
        raise InvalidArgument

    max_diff = 0
    for i in range(len(lst) - 1):
        if not isinstance(lst[i], int) or not isinstance(lst[i + 1], int):
            raise TypeError
        diff = abs(lst[i] - lst[i + 1])
        if diff > max_diff:
            max_diff = diff

    return max_diff
